/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.CarsMSFT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.Action;

/**
 *
 * @author 1796111
 */
public class Database {
    private Connection conn;
    
    public Database() throws SQLException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");                    
        } catch (ClassNotFoundException ex) {
            // exception chaining
            throw new SQLException("Driver not found", ex);
        }
        String url = "jdbc:sqlserver://myjac.database.windows.net:1433;database=myJacDB;user=jing@myjac;password={1qaz@WSX};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
        conn = DriverManager.getConnection(url);                             
    }
    
    public Car getCarById(long id) throws SQLException{
        String sql = "SELECT * FROM cars WHERE id = ?";
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Car(result.getLong("id"), result.getString("makeModel"), result.getBigDecimal("engineSize"), Car.FuelType.valueOf(result.getString("fuelType")));
            } else {
                return null;
            }
        }
    }
   
    public ArrayList<Car> getAllCars() throws SQLException {
        String sql = "SELECT * FROM cars";
        ArrayList<Car> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                long id = result.getLong("id");
                String makeModel = result.getString("makeModel");
                BigDecimal engineSize = result.getBigDecimal("engineSize");
                Car.FuelType fuelType = Car.FuelType.valueOf(result.getString("fuelType"));
                
                Car car = new Car(id, makeModel, engineSize, fuelType);
                list.add(car);
            }
        }
        return list;
    }
    
    public void addCar(Car car) throws SQLException {
        String sql = "INSERT INTO cars (makeModel, engineSize, fuelType) VALUES(?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, car.getMakeModel());
            //stmt.setDouble(2, 2.4);
            stmt.setDouble(2, car.getEngineSize().doubleValue());
            stmt.setString(3, car.getFuelType().toString());
            
            stmt.executeUpdate();
        } 
    }
    
    
    public void updateCar(Car car) throws SQLException {
        String sql = "UPDATE cars SET makeModel = ?, engineSize = ?, fuelType = ? WHERE id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, car.getMakeModel());
            stmt.setDouble(2, car.getEngineSize().doubleValue());
            stmt.setString(3, car.getFuelType().toString());
            stmt.setLong(4, car.getId());
            
            stmt.executeUpdate();
        }
    }
    
    public void deleteCar(long id) throws SQLException {
        String sql = "DELETE FROM cars WHERE id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            
            stmt.executeUpdate();
        }
    }
    
    /**
     * @param conn the conn to set
     */
    public void setAutoCommit(boolean flag) throws SQLException {
        conn.setAutoCommit(flag); 
    }
    
    public void commitUpdate() throws SQLException {
        conn.commit();
    }

    public void rollbackUpdate() throws SQLException {
        conn.rollback();
    }
    
    
    
}
