package ipd12.CarsMSFT;

import java.math.BigDecimal;

/**
 *
 * @author 1796111
 */
public class Car {
    private long id;
    private String makeModel;
    private BigDecimal engineSize;
    private FuelType fuelType;
    
    // enum for fuel type
    public static enum FuelType { Gasoline, Diesel, Propane, Other }

    // constructor
    public Car(long id, String makeModel, BigDecimal engineSize, FuelType fuelType) {
        this.id = id;
        this.makeModel = makeModel;
        this.engineSize = engineSize;
        this.fuelType = fuelType;
    }

    // setters and getters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public BigDecimal getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(BigDecimal engineSize) {
        this.engineSize = engineSize;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }
    
    @Override
    public String toString() {
        return String.format("%d:  %s  %.1f  %s", id, makeModel, engineSize, fuelType);
    }
    
    
    
}
