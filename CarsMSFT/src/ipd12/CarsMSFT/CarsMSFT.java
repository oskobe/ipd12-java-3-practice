/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.CarsMSFT;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

class CustomFilterCSV extends javax.swing.filechooser.FileFilter {

    @Override
    public boolean accept(File file) {
        // Allow only directories, or files with ".txt" extension
        return file.isDirectory() || file.getAbsolutePath().endsWith(".csv");
    }

    @Override
    public String getDescription() {
        // This description will be displayed in the dialog,
        // hard-coded = ugly, should be done via I18N
        return "CSV documents (*.csv)";
    }
}

/**
 *
 * @author 1796111
 */
public class CarsMSFT extends javax.swing.JFrame {

    DefaultListModel<Car> modelCarsList = new DefaultListModel<>();
    Database db;
    Car selectedCar;
    // for 2 different file chooser types, 1: save; 2: open
    private static char FILE_CHOOSER_TYPE_SAVE = 'S';
    private static char FILE_CHOOSER_TYPE_OPEN = 'O';

    /**
     * Creates new form CarsMSFT
     */
    public CarsMSFT() {
        try {
            // connect to db
            db = new Database();
            initComponents();
            reloadCars();
        }
        catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Error connecting to database: " + e.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            dispose(); // can't continue if database connection failed
        }

    }

    private void reloadCars() {
        try {
            // initialization
            //lblId.setText("...");
            //tfName.setText("");
            //rbMale.setSelected(true);
            selectedCar = null;
            modelCarsList.clear();

            // get all friends
            ArrayList<Car> list = db.getAllCars();
            for (Car c : list) {
                modelCarsList.addElement(c);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Error fetching data: " + e.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popMenu = new javax.swing.JPopupMenu();
        miEdit = new javax.swing.JMenuItem();
        miDelete = new javax.swing.JMenuItem();
        dlgAddEdit = new javax.swing.JDialog();
        jLabel8 = new javax.swing.JLabel();
        dlgAddEdit_lblId = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        dlgAddEdit_lblEngineSize = new javax.swing.JLabel();
        dlgAddEdit_cbFuelType = new javax.swing.JComboBox<>();
        dlgAddEdit_tfMakeModel = new javax.swing.JTextField();
        dlgAddEdit_sldEngineSize = new javax.swing.JSlider();
        dlgAddEdit_btCancel = new javax.swing.JButton();
        dlgAddEdit_btSave = new javax.swing.JButton();
        fcExportToCSV = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstCarList = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miExportToCSV = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();
        mAdd = new javax.swing.JMenu();

        miEdit.setText("Edit");
        miEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miEditActionPerformed(evt);
            }
        });
        popMenu.add(miEdit);

        miDelete.setText("Delete");
        miDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDeleteActionPerformed(evt);
            }
        });
        popMenu.add(miDelete);

        dlgAddEdit.setTitle("Add / Edit Car");
        dlgAddEdit.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        dlgAddEdit.setModal(true);

        jLabel8.setText("ID:");

        dlgAddEdit_lblId.setText("...");

        jLabel10.setText("Make Model:");

        jLabel11.setText("Engine Size:");

        jLabel12.setText("Fuel Type:");

        dlgAddEdit_lblEngineSize.setText("3.5 L");

        dlgAddEdit_cbFuelType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gasoline", "Diesel", "Propane", "Other" }));

        dlgAddEdit_tfMakeModel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_tfMakeModelActionPerformed(evt);
            }
        });

        dlgAddEdit_sldEngineSize.setMajorTickSpacing(50);
        dlgAddEdit_sldEngineSize.setMaximum(200);
        dlgAddEdit_sldEngineSize.setMinorTickSpacing(10);
        dlgAddEdit_sldEngineSize.setPaintLabels(true);
        dlgAddEdit_sldEngineSize.setPaintTicks(true);
        dlgAddEdit_sldEngineSize.setValue(35);
        dlgAddEdit_sldEngineSize.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                dlgAddEdit_sldEngineSizeStateChanged(evt);
            }
        });

        dlgAddEdit_btCancel.setText("Cancel");
        dlgAddEdit_btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btCancelActionPerformed(evt);
            }
        });

        dlgAddEdit_btSave.setText("Save");
        dlgAddEdit_btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgAddEditLayout = new javax.swing.GroupLayout(dlgAddEdit.getContentPane());
        dlgAddEdit.getContentPane().setLayout(dlgAddEditLayout);
        dlgAddEditLayout.setHorizontalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel10)
                            .addComponent(jLabel8))
                        .addGap(38, 38, 38)
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dlgAddEdit_lblId)
                            .addComponent(dlgAddEdit_lblEngineSize)
                            .addComponent(dlgAddEdit_sldEngineSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgAddEdit_tfMakeModel)
                            .addComponent(dlgAddEdit_cbFuelType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addComponent(dlgAddEdit_btCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dlgAddEdit_btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        dlgAddEditLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {dlgAddEdit_btCancel, dlgAddEdit_btSave});

        dlgAddEditLayout.setVerticalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dlgAddEdit_lblId, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8))
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(dlgAddEdit_tfMakeModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(dlgAddEdit_lblEngineSize)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dlgAddEdit_sldEngineSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(23, 23, 23)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(dlgAddEdit_cbFuelType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
<<<<<<< HEAD
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
=======
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
>>>>>>> 6e0fd38fd322924381e0b0971594523838564079
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_btCancel)
                    .addComponent(dlgAddEdit_btSave))
                .addGap(30, 30, 30))
        );

        fcExportToCSV.setFileFilter(new CustomFilterCSV());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Car List");
        setName("frmCarList"); // NOI18N

        jLabel1.setText("jLabel1");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(258, 130));

        lstCarList.setModel(modelCarsList);
        lstCarList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstCarList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstCarListMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstCarListMouseReleased(evt);
            }
        });
        lstCarList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstCarListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstCarList);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                .addContainerGap())
        );

        jMenu1.setText("File");

        miExportToCSV.setText("Export to CSV ...");
        miExportToCSV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExportToCSVActionPerformed(evt);
            }
        });
        jMenu1.add(miExportToCSV);

        miExit.setText("Exit");
        miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExitActionPerformed(evt);
            }
        });
        jMenu1.add(miExit);

        jMenuBar1.add(jMenu1);

        mAdd.setText("Add");
        mAdd.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                mAddMenuSelected(evt);
            }
        });
        mAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mAddMouseClicked(evt);
            }
        });
        jMenuBar1.add(mAdd);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void miEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miEditActionPerformed
        openAddEditDialog(selectedCar);
    }//GEN-LAST:event_miEditActionPerformed

    private void miDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDeleteActionPerformed
        // delete confirmation
        if (selectedCar != null) {
            Object[] options = {"Cancel", "Delete"};
            int decision = JOptionPane.showOptionDialog(this,
                    "Are you sure you want to delete the car: "
                    + String.format("ID# %d:   \"%s\"", selectedCar.getId(), selectedCar.getMakeModel()),
                    "Confirm delete",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null, //do not use a custom Icon
                    options, //the titles of buttons
                    options[0]); //default button title

            if (decision == 1) {
                // delete from database
                try {
                    db.deleteCar(selectedCar.getId());
                    reloadCars();
                }
                catch (SQLException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(this, "Error: car deletion error !", "Database error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

    }//GEN-LAST:event_miDeleteActionPerformed

    private void lstCarListMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCarListMouseReleased
        if (evt.isPopupTrigger()) {
            popMenu.show(lstCarList, evt.getX(), evt.getY());
            int idx = lstCarList.locationToIndex(evt.getPoint());
            lstCarList.setSelectedIndex(idx);
            //Car car = modelCarsList.getElementAt(idx);
        }
    }//GEN-LAST:event_lstCarListMouseReleased

    private void lstCarListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCarListMouseClicked
        if (evt.getClickCount() == 2) {
            openAddEditDialog(selectedCar);
        }
    }//GEN-LAST:event_lstCarListMouseClicked

    private void lstCarListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstCarListValueChanged
        selectedCar = lstCarList.getSelectedValue();
    }//GEN-LAST:event_lstCarListValueChanged

    private void dlgAddEdit_tfMakeModelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_tfMakeModelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dlgAddEdit_tfMakeModelActionPerformed

    private void dlgAddEdit_sldEngineSizeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_dlgAddEdit_sldEngineSizeStateChanged
        double engineSize = (double) (dlgAddEdit_sldEngineSize.getValue()) / 10;
        dlgAddEdit_lblEngineSize.setText(String.format("%.1f L", engineSize));
    }//GEN-LAST:event_dlgAddEdit_sldEngineSizeStateChanged

    private void dlgAddEdit_btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btCancelActionPerformed
        dlgAddEdit.setVisible(false);
    }//GEN-LAST:event_dlgAddEdit_btCancelActionPerformed

    private void dlgAddEdit_btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btSaveActionPerformed

        // get info from dialog screen
        String makeModel = dlgAddEdit_tfMakeModel.getText();
        BigDecimal engineSize = new BigDecimal((double) (dlgAddEdit_sldEngineSize.getValue()) / 10);
        Car.FuelType fuelType = Car.FuelType.valueOf(dlgAddEdit_cbFuelType.getSelectedItem().toString());
        
        // user did not enter make model
        if (makeModel.trim().compareTo("") == 0) {
            return;
        }
        
        // user did not choose engine size
        if (engineSize.compareTo(new BigDecimal(0)) == 0) {
            return;
        }

        // case: Add new car
        if (selectedCar == null) {
            try {
                Car car = new Car(0, makeModel, engineSize, fuelType);
                db.addCar(car);

                dlgAddEdit.setVisible(false);
                reloadCars();
            }
            catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, "Error: car add error !", "Database error", JOptionPane.ERROR_MESSAGE);
            }
            
        }
        // case: Edit old car
        else {
            try {
                Car car = new Car(selectedCar.getId(), makeModel, engineSize, fuelType);
                db.updateCar(car);

                dlgAddEdit.setVisible(false);
                reloadCars();
            }
            catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, "Error: car update error !", "Database error", JOptionPane.ERROR_MESSAGE);
            }

        }
    }//GEN-LAST:event_dlgAddEdit_btSaveActionPerformed

    private void mAddMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_mAddMenuSelected
        /*
        selectedCar = null;
        openAddEditDialog(selectedCar);
         */
    }//GEN-LAST:event_mAddMenuSelected

    private void mAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mAddMouseClicked
        selectedCar = null;
        openAddEditDialog(selectedCar);
    }//GEN-LAST:event_mAddMouseClicked

    private void miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_miExitActionPerformed

    private void miExportToCSVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExportToCSVActionPerformed
        try {
            ArrayList<Car> allCarsList = db.getAllCars();

            if (allCarsList.size() > 0) {
                exportCarsToCSV(allCarsList);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Database fetching error !" + ex.getMessage(),
                    "Database Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_miExportToCSVActionPerformed

    private void openAddEditDialog(Car car) {
        // case: New
        if (car == null) {
            dlgAddEdit.setTitle("Add new car");
            dlgAddEdit_lblId.setText("...");
            dlgAddEdit_tfMakeModel.setText("");
            dlgAddEdit_lblEngineSize.setText("2.0 L");
            dlgAddEdit_sldEngineSize.setValue(20);
            dlgAddEdit_cbFuelType.setSelectedIndex(0);

            // case: Edit
        }
        else {
            dlgAddEdit.setTitle("Edit your car");
            dlgAddEdit_lblId.setText(car.getId() + "");
            dlgAddEdit_tfMakeModel.setText(car.getMakeModel());
            dlgAddEdit_lblEngineSize.setText(car.getEngineSize() + " L");
            dlgAddEdit_sldEngineSize.setValue((int) (car.getEngineSize().doubleValue() * 10));
            dlgAddEdit_cbFuelType.setSelectedItem(car.getFuelType().toString());
        }

        Hashtable labelTable = new Hashtable();
        labelTable.put(new Integer(0), new JLabel("0"));
        labelTable.put(new Integer(50), new JLabel("5"));
        labelTable.put(new Integer(100), new JLabel("10"));
        labelTable.put(new Integer(150), new JLabel("15"));
        labelTable.put(new Integer(200), new JLabel("20"));
        dlgAddEdit_sldEngineSize.setLabelTable(labelTable);

        dlgAddEdit.pack();
        // set location for dialog
        //dlgAddEdit.setLocationRelativeTo(null);
        dlgAddEdit.setLocation(this.getX() + 300, this.getY() + 70);

        dlgAddEdit.setVisible(true);
    }

    private void exportCarsToCSV(ArrayList<Car> carsList) {

        // get file name
        File file = getFileName(fcExportToCSV, FILE_CHOOSER_TYPE_SAVE);

        if (file == null) {
            return;
        }

        String rowOfFile;
        FileWriter fwriter;

        try {

            fwriter = new FileWriter(file, false);

            try (PrintWriter outputFile = new PrintWriter(fwriter)) {
                for (Car car : carsList) {
                    rowOfFile = car.getId() + "," + car.getMakeModel() + "," + String.format("%.1f", car.getEngineSize()) + "," + car.getFuelType();
                    outputFile.println(rowOfFile);
                }
            }
            /*
            JOptionPane.showMessageDialog(this, "Data was exported to CSV file successfully !",
                    "Cancelled", JOptionPane.INFORMATION_MESSAGE);
             */
        }
        catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "File export error !" + ex.getMessage(),
                    "File Error", JOptionPane.ERROR_MESSAGE);
        }
        /*finally {
            outputFile.close();
        }*/

    }

    private File getFileName(JFileChooser fc, char fcType) {

        // set default directory
        File dir = new File(System.getProperty("user.dir") + "\\data\\");
        System.out.println(dir);
        fc.setCurrentDirectory(dir);

        // show file chooser for SAVE or OPEN 
        int returnVal = fcType == FILE_CHOOSER_TYPE_SAVE ? fc.showSaveDialog(this) : fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();

            /* use pattern
            Pattern p = Pattern.compile("^.*\\.([c|C][s|S][v|V])$");
            Matcher m = p.matcher(file.toString());
            boolean result = m.matches();
             */
 /* do not use pattern */
            boolean result = file.toString().matches("^.*\\.([c|C][s|S][v|V])$");

            if (!result) {
                String ext;
                if (file.toString().charAt(file.toString().length() - 1) == '.') {
                    ext = "csv";
                }
                else {
                    ext = ".csv";
                }
                file = new File(file.toString() + ext);
            }

            return file;
        }
        else {
            JOptionPane.showMessageDialog(this, "File chooser has been cancelled by user  !",
                    "Cancelled", JOptionPane.INFORMATION_MESSAGE);
            return null;
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CarsMSFT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CarsMSFT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CarsMSFT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CarsMSFT.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CarsMSFT().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog dlgAddEdit;
    private javax.swing.JButton dlgAddEdit_btCancel;
    private javax.swing.JButton dlgAddEdit_btSave;
    private javax.swing.JComboBox<String> dlgAddEdit_cbFuelType;
    private javax.swing.JLabel dlgAddEdit_lblEngineSize;
    private javax.swing.JLabel dlgAddEdit_lblId;
    private javax.swing.JSlider dlgAddEdit_sldEngineSize;
    private javax.swing.JTextField dlgAddEdit_tfMakeModel;
    private javax.swing.JFileChooser fcExportToCSV;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<Car> lstCarList;
    private javax.swing.JMenu mAdd;
    private javax.swing.JMenuItem miDelete;
    private javax.swing.JMenuItem miEdit;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miExportToCSV;
    private javax.swing.JPopupMenu popMenu;
    // End of variables declaration//GEN-END:variables
}
