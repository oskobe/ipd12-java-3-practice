/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package account;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wjing
 */
public class AccountTest {
    
    public AccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("test starting");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("test ending");
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        
    }

    /**
     * Test of getID method, of class Account.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        Account instance = new Account();
        instance.setID(100);
        int expResult = 100;
        int result = instance.getID();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setID method, of class Account.
     */
    @Test
    public void testSetID() {
        System.out.println("setID");
        int ID = 105;
        Account instance = new Account();
        instance.setID(ID);
        int expResult = 105;
        int result = instance.getID();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Account.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Account instance = new Account();
        instance.setName("Wang Jing");
        String expResult = "Jing WANG";
        String result = instance.getName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setName method, of class Account.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Jing WANG";
        Account instance = new Account();
        instance.setName(name);
        String expResult = "Jing WANG";
        String result = instance.getName();
        assertEquals(expResult, result);
    }
    
}
