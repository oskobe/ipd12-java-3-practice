
package geometry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javafx.print.Collation;

abstract class GeoObj implements Comparable<GeoObj>{
    String color;

    public GeoObj(String color) {
        this.color = color;
    }
    
    abstract double getSurface();

    @Override
    public int compareTo(GeoObj other) {
        System.out.printf("compare " + this.color + " VS " + other.color + "\n");
        return this.color.compareTo(other.color);
    }
    
    public final static SurfaceComparator sufaceComparator =  new SurfaceComparator();
}

class SurfaceComparator implements Comparator<GeoObj> {

    @Override
    public int compare(GeoObj o1, GeoObj o2) {
        if (o1.getSurface() == o2.getSurface() ) {
            return 0;
        } else if (o1.getSurface() > o2.getSurface()) {
            return 1;
        } else {
            return -1;
        }
        
    }

   
    
}


class Rectangle extends GeoObj {
    
    double width;
    double height;
    
    public Rectangle(String color, double width, double height) {
        super(color);
        this.width = width;
        this.height =height;
    }

    @Override
    double getSurface() {
        return width * height;
    }
    
}

// TODO define class circle in the same mamer with radius as constructor's additonal parameter

class Circle extends GeoObj {
    double radius;

    public Circle(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    @Override
    double getSurface() {
        return Math.PI * Math.pow(radius, 2);
    }
    
    
}

public class GeoMetry {

    public static void main(String[] args) {
        ArrayList<GeoObj> list = new ArrayList<>();
        
        list.add(new Rectangle("blue", 2, 3.2));
        list.add(new Circle("white", 7));
        list.add(new Rectangle("green", 12, 6));
        list.add(new Circle("purper", 2));
        
        
        System.out.println("Orignal order: ");
        for (GeoObj go : list) {
            System.out.printf("%s object has surface %.2f\n", go.color, go.getSurface());
        }
                
        Collections.sort(list);
        
        System.out.println("After order: ");
        for (GeoObj go : list) {
            System.out.printf("%s object has surface %.2f\n", go.color, go.getSurface());
        }
        
        Collections.sort(list, GeoObj.sufaceComparator);
        
        System.out.println("After order by surface: ");
        for (GeoObj go : list) {
            System.out.printf("%s object has surface %.2f\n", go.color, go.getSurface());
        }
    }
    
}
