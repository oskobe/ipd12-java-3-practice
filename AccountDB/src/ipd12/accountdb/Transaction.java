/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.accountdb;

import java.math.BigDecimal;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author 1796111
 */
public class Transaction 
{
    private long id;
    private BigDecimal deposit;
    private BigDecimal withdraw;
    private Date opDate;

    public Transaction(long id, BigDecimal deposit, BigDecimal withdraw, Date opDate)
    {
        setId(id);
        setDeposit(deposit);
        setWithdraw(withdraw);
        setOpDate(opDate);
    }
 
    /**
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(long id)
    {
        this.id = id;
    }

    /**
     * @return the deposit
     */
    public BigDecimal getDeposit()
    {
        return deposit;
    }

    /**
     * @param deposit the deposit to set
     * @throws java.lang.IllegalAccessException
     */
    public final void setDeposit(BigDecimal deposit)  
    {

        if (deposit.compareTo(new BigDecimal(0)) < 0) {
            throw new IllegalArgumentException("Deposit must be a non-negative value");
        }
        this.deposit = deposit;
    }

    /**
     * @return the withdraw
     */
    public BigDecimal getWithdraw()
    {
        return withdraw;
    }

    /**
     * @param withdraw the withdraw to set
     */
    public final void setWithdraw(BigDecimal withdraw)
    {
        if (withdraw.compareTo(new BigDecimal(0)) < 0) {
            throw new IllegalArgumentException("Withdrawal must be a non-negative value");
        }
        this.withdraw = withdraw;
    }

    /**
     * @return the opDate
     */
    public Date getOpDate()
    {
        return opDate;
    }

    /**
     * @param opDate the opDate to set
     */
    public final void setOpDate(Date opDate)
    {
        this.opDate = opDate;
    }

    public static final SimpleDateFormat sdf = new SimpleDateFormat("MM-d-yyyy");
    
    @Override
    public String toString() {
        
        String dateStr = sdf.format(opDate);
        
        String tran = deposit.compareTo(withdraw) == 1 ? "Deposited  $" + deposit : "Withdrawed  $" + withdraw;
        return id + ": " + tran + " on " + dateStr;
    }
    
}
