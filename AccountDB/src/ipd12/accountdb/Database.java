/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.accountdb;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author 1796111
 */
public class Database
{
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "account";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";

    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);        
    }
    
    
    
    public void addTransaction(Transaction transaction) throws SQLException {
        String sql = "INSERT INTO accounts (deposit, withdraw) VALUES (?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, transaction.getDeposit());
            stmt.setBigDecimal(2, transaction.getWithdraw()); 
            stmt.executeUpdate();
        }
    }
    
    public ArrayList<Transaction> getAllTransactions() throws SQLException {
        String sql = "SELECT * FROM accounts";
        ArrayList<Transaction> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                BigDecimal deposit = result.getBigDecimal("deposit");
                BigDecimal withdraw = result.getBigDecimal("withdraw");
                //java.sql.Date opDateSql = result.getDate("opDate");
                java.sql.Date opDateSql = new java.sql.Date(result.getDate("opDate").getTime());

                Transaction transaction = new Transaction(id, deposit, withdraw, opDateSql);
                list.add(transaction);
            }
        }
        return list;
    }
    
    public Transaction getTransactionById(int id) throws SQLException {
        // FIXME: Preapred statement is required if id may contain malicious SQL injection code
        String sql = "SELECT * FROM accounts WHERE id=" + id;
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                BigDecimal deposit = result.getBigDecimal("deposit");
                BigDecimal withdraw = result.getBigDecimal("withdraw");
                java.sql.Date opDateSql = result.getDate("opDate");
                //java.sql.Date opDateSql = new java.sql.Date(result.getDate("opDate").getTime());

                Transaction transaction = new Transaction(id, deposit, withdraw, opDateSql);
                return transaction;
            } else {
                throw new SQLException("Record not found");
                //return null;
            }
        }
    }
    
    public void updateTransaction(Transaction transaction) throws SQLException {
        String sql = "UPDATE accounts SET deposit=?, withdraw=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setBigDecimal(1, transaction.getDeposit());
            stmt.setBigDecimal(2, transaction.getWithdraw());
            // where is the last parameter
            stmt.setLong(3, transaction.getId());
            
            stmt.executeUpdate();
        }
    }
    
    public void deleteTransactionById(int id) throws SQLException {
        String sql = "DELETE FROM accounts WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);            
            stmt.executeUpdate();
        }
    }

   public BigDecimal getBalance() throws SQLException {
        String sql = "SELECT SUM(deposit-withdraw) as sum FROM accounts";
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                BigDecimal sum = result.getBigDecimal("sum");
                return sum;
            } else {
                return new BigDecimal(0); // no records found
            }
        }
    }

    /**
     * @param conn the conn to set
     */
    public void setAutoCommit(boolean flag) throws SQLException {
        conn.setAutoCommit(flag); 
    }
    
    /**
     * 
     */
    public void commitUpdate() throws SQLException {
        conn.commit();
    }
    
    /**
     * 
     */
    public void rollbackUpdate() throws SQLException {
        conn.rollback();
    }
    
}
