
package bestcar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;


class Car implements Comparable<Car>{
    String make;
    String model;
    int maxSpeedKmph;
    double setTo100Kmph;
    double litersPer100km;
    public final static CarComparatorByMaxSpeed carComparatorByMaxSpeed = new CarComparatorByMaxSpeed();
    public final static CarComparatorByAccelleration carComparatorByAccelleration = new CarComparatorByAccelleration();
    public final static CarComparatorByEconomy carComparatorByEconomy = new CarComparatorByEconomy();
            
    public Car(){
        
    }

    public Car(String make, String model, int maxSpeedKmph, double setTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.setTo100Kmph = setTo100Kmph;
        this.litersPer100km = litersPer100km;
    }

    @Override
    public int compareTo(Car other) {
        if (this.make.compareToIgnoreCase(other.make) == 0) {
            return this.model.compareToIgnoreCase(other.model);
        } else {
            return this.make.compareToIgnoreCase(other.make);
        }
    }

    public String toString() {
        return String.format("%s/%s/%d/%.2f/%.2f \n", this.make, this.model, this.maxSpeedKmph, this.setTo100Kmph, this.litersPer100km);
    }
}


// CarComparatorByMaxSpeed
class CarComparatorByMaxSpeed implements Comparator<Car> {

    @Override
    public int compare(Car o1, Car o2) {
      return o1.maxSpeedKmph - o2.maxSpeedKmph;
    }
    
}

// CarComparatorByAccelleration
class CarComparatorByAccelleration implements Comparator<Car> {

    @Override
    public int compare(Car o1, Car o2) {
        if (o1.setTo100Kmph == o2.setTo100Kmph) {
            return 0;
        } else if (o1.setTo100Kmph > o2.setTo100Kmph) {
            return 1;
        } else {
            return -1;
        }     
    }
    
}

// CarComparatorByEconomy
class CarComparatorByEconomy implements Comparator<Car> {

    @Override
    public int compare(Car o1, Car o2) {
        if (o1.litersPer100km == o2.litersPer100km) {
            return 0;
        } else if (o1.litersPer100km > o2.litersPer100km) {
            return 1;
        } else {
            return -1;
        } 
    }
    
}

public class BestCar {

    static ArrayList<Car> garage = new ArrayList<>();
    
    public static void main(String[] args) {
        garage.add(new Car("Toyota", "Sierra", 137, 16.3, 18.8));
        garage.add(new Car("Ford", "Escape", 147, 16.2, 11.5));
        garage.add(new Car("Audi", "A5", 199, 11.0, 12.0)); 
        garage.add(new Car("Toyota", "Corolla", 156, 14.3, 8.9));
        garage.add(new Car("BMW", "X5", 210, 8.9, 15.3));
        garage.add(new Car("Audi", "A1", 140, 13.5, 9.5));
        garage.add(new Car("BENZ", "LS500", 190, 9.0, 14.8));
        garage.add(new Car("Ford", "Edge", 180, 12.5, 13.5));
        garage.add(new Car("GMC", "Terrain", 152, 16.5, 10.6));
        garage.add(new Car("Toyota", "Matrix", 138, 16.3, 6.8));
        
        Collections.sort(garage);
        System.out.println("== The List of all Car ==");
        for (Car car : garage) {
            System.out.println(car);
        }
        
        // the best and worst car according to creteria: CarComparatorByMaxSpeed
        Collections.sort(garage, Car.carComparatorByMaxSpeed);
        System.out.println("== The best car of Max Speed is: " + garage.get(garage.size() - 1));
        System.out.println("== The worst car of Max Speed is: " + garage.get(0));
        
        // the best and worst car according to creteria: CarComparatorByAccelleration
        Collections.sort(garage, Car.carComparatorByAccelleration);
        System.out.println("== The best car of Accelleration is: " + garage.get(0));
        System.out.println("== The worst car of Accelleration is: " + garage.get(garage.size() - 1));
        
        // the best and worst car according to creteria: CarComparatorByEconomy
        Collections.sort(garage, Car.carComparatorByEconomy);
        System.out.println("== The best car of Economy is: " + garage.get(0));
        System.out.println("== The worst car of Economy is: " + garage.get(garage.size() - 1));
        
    }
    

}
