/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.todo;

import java.sql.Date;

/**
 *
 * @author 1796111
 */
public class Todo
{
    long id;
    String task;
    Date dueDate;
    boolean isDone;

    public Todo()
    {
    }

    public Todo(long id, String task, Date dueDate, boolean isDone)
    {
        this.id = id;
        this.task = task;
        this.dueDate = dueDate;
        this.isDone = isDone;
    }

    @Override
    public String toString() {
        
        String isDoneTxt = (isDone)? "Done" : "Pending";
        String dueDateTxt = dueDate.toString().replaceAll("-", "/");

        return id + ":  " + task + "        " + dueDateTxt + "        " + isDoneTxt;
    }
}
