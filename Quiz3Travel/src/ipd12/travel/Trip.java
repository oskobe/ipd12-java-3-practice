/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.travel;

import java.sql.Date;

/**
 *
 * @author 1796111
 */
public class Trip
{
    // define variables
    long id;
    String name;
    Date depDate;
    Date retDate;
    String dest;
    boolean isFirstClass;

    // constructor
    public Trip()
    {
    }

    public Trip(long id, String name, Date depDate, Date retDate, String dest, boolean isFirstClass)
    {
        this.id = id;
        this.name = name;
        this.depDate = depDate;
        this.retDate = retDate;
        this.dest = dest;
        this.isFirstClass = isFirstClass;
    }
    
    //
    @Override
    public String toString() {
        
        // declare variables, display dates user friendly
        String depDateTxt = depDate.toString().replaceAll("-", "/");
        String retDateTxt = retDate.toString().replaceAll("-", "/");
        // display text depends on if it is first class
        String isFirstClassTxt = (isFirstClass)? "First Class": "NOT First Class";
        
        return String.format("%d: %s travel to %s on %s and return on %s, %s", 
                id, name, dest, depDateTxt, retDateTxt, isFirstClassTxt);
    }
    
}
