package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;

class Person implements Comparable<Person> {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Person other) {
         return this.age - other.age;
    } 
}

public class EffectiveSorting {
    static ArrayList<Person> people = new ArrayList<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        people.add(new Person("Janet", 23));
        people.add(new Person("White", 78));
        people.add(new Person("Brain", 45));
        people.add(new Person("Smith", 34));
        people.add(new Person("Alexandre", 45));
        people.add(new Person("Michelle", 28));
        people.add(new Person("Daniel", 10));
        
        Collections.sort(people);
        
        for (Person p : people) {
            System.out.printf("%s/%d \n", p.name, p.age);
        }
    }
    
}
