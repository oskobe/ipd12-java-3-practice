/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homwork20180213;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jdk.nashorn.internal.objects.NativeString;

/**
 *
 * @author 1796111
 */
public class Registration extends javax.swing.JFrame
{

    // CONNECTION
    Connection conn;
    /**
     * Creates new form Registration
     */
    
    
    public Registration()
    {
        

        try {
            
            String dbURL = "jdbc:mysql://localhost:3306/school";
            String username = "root";
            String password = "root";
            // CONNECTION
            conn = (Connection) DriverManager.getConnection(dbURL, username, password);
            
            initComponents();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Fatal error: unable to connect to database !", "DB error", JOptionPane.ERROR_MESSAGE);
            
            System.exit(1);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        btnGrpAge = new javax.swing.ButtonGroup();
        lblRegistrationForm = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblAge = new javax.swing.JLabel();
        lblPets = new javax.swing.JLabel();
        lblContinent = new javax.swing.JLabel();
        lblPreferTemp = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        rbLess18 = new javax.swing.JRadioButton();
        rbBtw18n35 = new javax.swing.JRadioButton();
        rbMore36 = new javax.swing.JRadioButton();
        chxCat = new javax.swing.JCheckBox();
        chxDog = new javax.swing.JCheckBox();
        chxOther = new javax.swing.JCheckBox();
        cbContinent = new javax.swing.JComboBox<>();
        sldTemp = new javax.swing.JSlider();
        btnRegister = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("User Registration");

        lblRegistrationForm.setText("Registration Form");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblName.setText("Name:");

        lblAge.setText("Age:");

        lblPets.setText("Pets:");

        lblContinent.setText("Continent of Residence:");

        lblPreferTemp.setText("Preferred Temperature (℃): ");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnGrpAge.add(rbLess18);
        rbLess18.setSelected(true);
        rbLess18.setText("Below 18");

        btnGrpAge.add(rbBtw18n35);
        rbBtw18n35.setText("18 - 35");

        btnGrpAge.add(rbMore36);
        rbMore36.setText("36 and up");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(rbLess18)
                .addGap(27, 27, 27)
                .addComponent(rbBtw18n35)
                .addGap(27, 27, 27)
                .addComponent(rbMore36)
                .addGap(0, 30, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbLess18)
                    .addComponent(rbBtw18n35)
                    .addComponent(rbMore36))
                .addContainerGap())
        );

        chxCat.setText("Cat");

        chxDog.setText("Dog");

        chxOther.setText("Other");

        cbContinent.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Asia", "Africa", "Antractica", "Australia", "Europe", "North America", "South America" }));

        sldTemp.setMajorTickSpacing(5);
        sldTemp.setMaximum(35);
        sldTemp.setMinimum(15);
        sldTemp.setMinorTickSpacing(1);
        sldTemp.setPaintLabels(true);
        sldTemp.setPaintTicks(true);
        sldTemp.setToolTipText("");
        sldTemp.setValue(26);
        sldTemp.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        sldTemp.setInheritsPopupMenu(true);
        sldTemp.setName(""); // NOI18N

        btnRegister.setText("Register Me");
        btnRegister.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRegisterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName)
                    .addComponent(lblAge)
                    .addComponent(lblPets)
                    .addComponent(lblPreferTemp)
                    .addComponent(lblContinent))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sldTemp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbContinent, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(chxCat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chxDog)
                        .addGap(59, 59, 59)
                        .addComponent(chxOther)
                        .addGap(39, 39, 39))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfName)
                    .addComponent(btnRegister, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(245, 245, 245))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(lblAge))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(lblPets))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chxOther)
                            .addComponent(chxDog)
                            .addComponent(chxCat))))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cbContinent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblContinent))
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sldTemp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblPreferTemp)))
                .addGap(42, 42, 42)
                .addComponent(btnRegister)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblRegistrationForm)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRegistrationForm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
        // declare variables 
        String  name = null, 
                age = null, 
                pets = "", 
                continent = null, 
                fileRecord = null;
        int preferTemp = 0;
       
        // get name
        name = tfName.getText();
        
        if (name == null || name.trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Please enter the name !", "Input error", JOptionPane.ERROR_MESSAGE);
            tfName.setText("");
            tfName.grabFocus();
        } else {
        
            // get age
            if (rbLess18.isSelected()) 
                age = rbLess18.getText();
            else if (rbBtw18n35.isSelected())
                age = rbBtw18n35.getText();
            else if (rbMore36.isSelected())
                age = rbMore36.getText();

            // get pets
            if (chxCat.isSelected()) {
                pets = concatPets(pets, chxCat.getText());
            }

            if (chxDog.isSelected()) {
                pets = concatPets(pets, chxDog.getText());
            }

            if (chxOther.isSelected()) {
                pets = concatPets(pets, chxOther.getText());
            }

            // get continent
            continent = cbContinent.getSelectedItem().toString();

            // get prefered temperature
            preferTemp = sldTemp.getValue();

            // concat the final record for appending to file
            fileRecord = String.join(";", name, age, pets, continent, Integer.toString(preferTemp));

            //JOptionPane.showMessageDialog(this, fileRecord);

            String fileName = "data/registration.txt";
            FileWriter fwriter;
            PrintWriter outputFile = null;
                    
            try {
                fwriter = new FileWriter(fileName, true);
                outputFile = new PrintWriter(fwriter);   

                outputFile.println(fileRecord);
                
                JOptionPane.showMessageDialog(this, "One record has been written in the file succussfully !", 
                        "Success", JOptionPane.INFORMATION_MESSAGE);
            }
            catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "File writing error !", "Error", JOptionPane.ERROR_MESSAGE);
            } finally {
                outputFile.close();
            }
            
            
            // save data to DB
            
            // INSERT
                
            String sql = "INSERT INTO preferences (name, ageRange, pets, continent, prefTemp) "
                    + "VALUES (?, ?, ?, ?, ?)";
 
            PreparedStatement statement;
            try
            {
                statement = conn.prepareStatement(sql);
                statement.setString(1, name);
                statement.setString(2, age);
                statement.setString(3, pets);
                statement.setString(4, continent);
                statement.setInt(5, preferTemp);

                //System.out.println(statement);


                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    JOptionPane.showMessageDialog(this, "One record has been written in the DB succussfully !", 
                        "Success", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (SQLException ex)
            {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, "Database writing error:\t" + ex.getMessage(), "DB error", JOptionPane.ERROR_MESSAGE);
            } 
            
            
        }
    
        
        
        
    }//GEN-LAST:event_btnRegisterActionPerformed

    private static String concatPets(String fstPart, String sndPart) {
        /*
        if (fstPart == "") {
            return sndPart;
        } else {
            return String.join(",", fstPart, sndPart);
        }
        */
        return (fstPart.equals("")) ? sndPart : String.join(",", fstPart, sndPart); 
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new Registration().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnGrpAge;
    private javax.swing.JButton btnRegister;
    private javax.swing.JComboBox<String> cbContinent;
    private javax.swing.JCheckBox chxCat;
    private javax.swing.JCheckBox chxDog;
    private javax.swing.JCheckBox chxOther;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblAge;
    private javax.swing.JLabel lblContinent;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPets;
    private javax.swing.JLabel lblPreferTemp;
    private javax.swing.JLabel lblRegistrationForm;
    private javax.swing.JRadioButton rbBtw18n35;
    private javax.swing.JRadioButton rbLess18;
    private javax.swing.JRadioButton rbMore36;
    private javax.swing.JSlider sldTemp;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}
