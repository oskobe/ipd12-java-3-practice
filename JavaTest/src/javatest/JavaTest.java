/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatest;

/**
 *
 * @author 1796111
 */
public class JavaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        genderEnum gender;
        
        gender = genderEnum.valueOf("Female");
        System.out.println(gender.ordinal());
        
        if (gender == genderEnum.Female) {
            System.out.println("xxx");
        }
        
        switch (gender) {
        case Male:
            System.out.println("yyyy");
        } 
        
        
        Vehicle vehicle = new Vehicle() {
            @Override
            public String toString() {
                return "AHA, YOU ARE A BIG BANG！";
            }
        };
        vehicle.setId(10001);
        vehicle.setName("BMW X5");
        System.out.println(vehicle);
        System.out.println(vehicle.toString());
        
        
    }
    
    enum genderEnum {
        Male, Female, NA
    }
    
    
    
}
