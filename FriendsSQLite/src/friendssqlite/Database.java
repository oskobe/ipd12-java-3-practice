/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package friendssqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.Action;

/**
 *
 * @author 1796111
 */
public class Database {
    private Connection conn;
    
    public Database() throws SQLException {
        String url = "jdbc:sqlite:E:/SQLite/ipd12.db";
        conn = DriverManager.getConnection(url);                             
    }
    
    public ArrayList<Friend> getAllFriends() throws SQLException {
        String sql = "SELECT * FROM friend";
        ArrayList<Friend> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String gender = result.getString("gender");
                
                Friend friend = new Friend(id, name, gender);
                list.add(friend);
            }
        }
        return list;
    }
    
    public Friend getFriendById(int id) throws SQLException{
        String sql = "SELECT * FROM friend WHERE id = ?";
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Friend(result.getInt("id"), result.getString("name"), result.getString("gender"));
            } else {
                return null;
            }
        }
    }
    
    public void addFriend(Friend friend) throws SQLException {
        String sql = "INSERT INTO friend (name, gender) VALUES(?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, friend.getName());
            stmt.setString(2, friend.getGender());
            
            stmt.executeUpdate();
        } 
    }
    
    public void updateFriend(Friend friend) throws SQLException {
        String sql = "UPDATE friend SET name = ?, gender = ? WHERE id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, friend.getName());
            stmt.setString(2, friend.getGender());
            stmt.setInt(3, friend.getId());
            
            stmt.executeUpdate();
        }
    }
    
    public void deleteFriend(int id) throws SQLException {
        String sql = "DELETE FROM friend WHERE id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);
            
            stmt.executeUpdate();
        }
    }
    
    /**
     * @param conn the conn to set
     */
    public void setAutoCommit(boolean flag) throws SQLException {
        conn.setAutoCommit(flag); 
    }
    
    public void commitUpdate() throws SQLException {
        conn.commit();
    }

    public void rollbackUpdate() throws SQLException {
        conn.rollback();
    }
}
