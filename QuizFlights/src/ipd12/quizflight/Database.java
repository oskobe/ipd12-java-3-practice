/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipd12.quizflight;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Action;

/**
 *
 * @author 1796111
 */
public class Database {
    private Connection conn;
    
    public Database() throws SQLException {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");                    
        } catch (ClassNotFoundException ex) {
            // exception chaining
            throw new SQLException("Driver not found", ex);
        }
        String url = "jdbc:sqlserver://myjac.database.windows.net:1433;database=myJacDB;user=jing@myjac;password={1qaz@WSX};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
        conn = DriverManager.getConnection(url);                             
    }
    
    public Flight getFlightById(long id) throws SQLException{
        String sql = "SELECT * FROM flights WHERE id = ?";
        
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Flight(result.getLong("id"), 
                        result.getDate("onDay"), 
                        result.getString("fromCode"), 
                        result.getString("toCode"),
                        Flight.Type.valueOf(result.getString("type")),
                        result.getInt("passengers"));
            } else {
                return null;
            }
        }
    }
   
    public ArrayList<Flight> getAllFlights() throws SQLException {
        String sql = "SELECT * FROM flights";
        ArrayList<Flight> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                long id = result.getLong("id");
                Date onDay = result.getDate("onDay");
                String fromCode = result.getString("fromCode");
                String toCode = result.getString("toCode");
                Flight.Type type = Flight.Type.valueOf(result.getString("type"));
                int passengers = result.getInt("passengers");
                
                Flight flight = new Flight(id, onDay, fromCode, toCode, type, passengers);
                list.add(flight);
            }
        }
        return list;
    }
    
    public void addFlight(Flight flight) throws SQLException {
        String sql = "INSERT INTO flights (onDay, fromCode, toCode, type, passengers) VALUES(?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setDate(1, new java.sql.Date(flight.getOnday().getTime()));
            stmt.setString(2, flight.getFromCode());
            stmt.setString(3, flight.getToCode());
            stmt.setString(4, flight.getType().toString());
            stmt.setInt(5, flight.getPassengers());
            
            stmt.executeUpdate();
        } 
    }
    
    
    public void updateFlight(Flight flight) throws SQLException {
        String sql = "UPDATE flights SET onDay = ?, fromCode = ?, toCode = ?, type = ?, passengers = ? WHERE id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setDate(1, new java.sql.Date(flight.getOnday().getTime()));
            stmt.setString(2, flight.getFromCode());
            stmt.setString(3, flight.getToCode());
            stmt.setString(4, flight.getType().toString());
            stmt.setInt(5, flight.getPassengers());
            stmt.setLong(6, flight.getId());
            
            stmt.executeUpdate();
        }
    }
    
    public void deleteFlight(long id) throws SQLException {
        String sql = "DELETE FROM flights WHERE id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            
            stmt.executeUpdate();
        }
    }
    
    /**
     * @param conn the conn to set
     */
    public void setAutoCommit(boolean flag) throws SQLException {
        conn.setAutoCommit(flag); 
    }
    
    public void commitUpdate() throws SQLException {
        conn.commit();
    }

    public void rollbackUpdate() throws SQLException {
        conn.rollback();
    }
    
    
    
}
