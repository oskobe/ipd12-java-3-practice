package ipd12.quizflight;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author 1796111
 */
public class Flight {
    private long id;
    private Date onDay;
    private String fromCode, toCode;
    Type type;
    int passengers;
    
    // enum for flight type
    enum Type { Domestic, International, Private }
    // use for date format
    public static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    // constructor
    public Flight(long id, Date onDay, String fromCode, String toCode, Type type, int passengers) {    
        this.id = id;
        this.onDay = onDay;
        this.fromCode = fromCode;
        this.toCode = toCode;
        this.type = type;
        this.passengers = passengers;
    }

    // setters and getters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getOnday() {
        return onDay;
    }

    public void setOnday(Date onDay) {
        this.onDay = onDay;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(String fromCode) {
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(String toCode) {
        this.toCode = toCode;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }   
    
    @Override
    public String toString() {
        String dateStr = sdf.format(onDay);
        return String.format("%d:  on %s  from %s to %s  %s flight with %d passengers", 
                id, sdf.format(onDay), fromCode, toCode, type, passengers);
    }
    
    
    
}
